package br.com.lf.abasteceali;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ferna on 14/12/2016.
 */

public class Data implements Serializable{

    public List<Cidade> cidades;
    public String status;
    @SerializedName("detail")
    public String detail;


}
