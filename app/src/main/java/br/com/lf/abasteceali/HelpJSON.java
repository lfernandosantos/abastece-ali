package br.com.lf.abasteceali;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.lf.abasteceali.models.Posto;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ferna on 28/01/2017.
 */

public class HelpJSON {
    List<Posto> postoList = new ArrayList<>();
    Activity activity;

    public HelpJSON(Activity activity){
        this.activity = activity;
    }

    public List<Posto> runJSON(LatLng endereco) {

        String latGPS = String.valueOf(endereco.latitude);
        String lonGPS = String.valueOf(endereco.longitude);

        OkHttpClient okHttpClient = getRequestHeader();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(IPostosJSON.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        IPostosJSON iPostosJSON = retrofit.create(IPostosJSON.class);

        String latitude = String.valueOf(latGPS);
        String longitude = String.valueOf(lonGPS);

        Call<Catalogo> listaCall = iPostosJSON.listaPostosLocation(latitude, longitude);

        Log.i("TAG", "Call: ");
        listaCall.enqueue(new Callback<Catalogo>() {
            @Override
            public void onResponse(Call<Catalogo> call, Response<Catalogo> response) {

                if (!response.isSuccessful()) {
                    Log.i("TAG", "ERRO: " + response.code());
                } else {
                    Catalogo catalogo = response.body();

                    for (Posto p : catalogo.postos) {
                        Log.i("TAG", "Posto: " + p.posto);
                        postoList.add(p);
                    }
                }
            }

            @Override
            public void onFailure(Call<Catalogo> call, Throwable t) {

                Log.i("TAG", "ERRO: " + t.getMessage() );

                new AlertDialog.Builder(activity)
                        .setTitle("Erro de Comunicação")
                        .setMessage("Houve erro ao buscar os postos. Verifique seu GPS, sua conexão e tente novamente!")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setCancelable(false)
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).create().show();
            }
        });

        return postoList;
    }

    private OkHttpClient getRequestHeader() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        return okHttpClient;
    }
}
