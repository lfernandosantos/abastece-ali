package br.com.lf.abasteceali;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

/**
 * Created by Fernando on 17/04/2016.
 */
public class Localizador {

    private Geocoder geo;
    public Localizador(Context context) {
        geo = new Geocoder(context);

    }

    public LatLng getCoordenadas(String endereco) {
        try {
            List<Address> listaEnderecos;
            listaEnderecos = geo.getFromLocationName(endereco, 1);
            if (!listaEnderecos.isEmpty()){
                Address address = listaEnderecos.get(0);
                return new LatLng(address.getLatitude(), address.getLongitude());
            }
            else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    public LatLng getCoordenada(String endereco) {
        try {
            List<Address> listaEnderecos;
            listaEnderecos = geo.getFromLocationName(endereco, 1);
            if (!listaEnderecos.isEmpty()){
                Address address = listaEnderecos.get(0);
                return new LatLng(address.getLatitude(), address.getLongitude());
            }
            else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
    public Address getEnderecoString(LatLng latLng){
        Address endereco = null;
        List<Address> listaEnderecos;
        try {

            listaEnderecos = geo.getFromLocation(latLng.latitude, latLng.longitude, 1);
            endereco = listaEnderecos.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return endereco;

    }
}
