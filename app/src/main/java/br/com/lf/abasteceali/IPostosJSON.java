package br.com.lf.abasteceali;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ferna on 14/12/2016.
 */

public interface IPostosJSON {

    String BASE_URL = "https://pure-brook-75560.herokuapp.com/api/postos/";

    @GET("location")
    Call<Catalogo> listaEstabs();

    @GET("location")
    Call<Catalogo> listaPostosLocation (@Query("latitude") String latitude, @Query("longitude") String longitude);

    @GET("location")
    Call<Catalogo> listaPostosLocationGNV (@Query("latitude") String latitude, @Query("longitude") String longitude,
                                           @Query("gnv") String gnv);

    @GET("location")
    Call<Catalogo> listaPostosLocationAlcool (@Query("latitude") String latitude, @Query("longitude") String longitude,
                                              @Query("alcool") String alcool);
}
