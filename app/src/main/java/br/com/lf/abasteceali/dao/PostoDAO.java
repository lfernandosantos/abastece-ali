package br.com.lf.abasteceali.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.security.AccessControlContext;
import java.util.ArrayList;
import java.util.List;

import br.com.lf.abasteceali.models.Posto;

import static java.security.AccessController.getContext;

/**
 * Created by lf.fernandodossantos on 14/04/17.
 */

public class PostoDAO extends SQLiteOpenHelper
{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "abasteceali.db";
    public static final String TABLE_NAME = "Postos";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " ( id INTEGER PRIMARY KEY, posto TEXT, endereco TEXT, bairro TEXT," +
                    "bandeira TEXT, gasolina TEXT, gnv TEXT, alcool TEXT," +
                    "latitude TEXT, longitude TEXT, cidade TEXT, uf TEXT);";

    public PostoDAO(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);

    }

    public void savePosto(Posto posto){

        Cursor c = getReadableDatabase().
                rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = " + posto.id + ";", null);

        //cadastro quando o posto não for encontrado
        if (!c.moveToNext()) {
            Log.i("DAO", "Salvo " + posto.posto);
            ContentValues values = new ContentValues();

            values.put("id", posto.id);
            values.put("posto", posto.posto);
            values.put("endereco", posto.endereco);
            values.put("bairro", posto.bairro);
            values.put("bandeira", posto.bandeira);
            values.put("gasolina", posto.gasolina);
            values.put("gnv", posto.gnv);
            values.put("alcool", posto.alcool);
            values.put("latitude", posto.latitude);
            values.put("longitude", posto.longitude);
            values.put("cidade", posto.cidade);
            values.put("uf", posto.uf);

            getWritableDatabase().insert(TABLE_NAME, null, values);
        }
    }

    public List<Posto> getPostos(){
        List<Posto> postos = new ArrayList<>();
        Cursor c  = getReadableDatabase().rawQuery("SELECT * FROM " + TABLE_NAME + ";", null);

        while (c.moveToNext()){
            Posto posto = new Posto();

            posto.id = c.getLong(c.getColumnIndex("id"));
            posto.posto = c.getString(c.getColumnIndex("posto"));
            posto.endereco = c.getString(c.getColumnIndex("endereco"));
            posto.bairro = c.getString(c.getColumnIndex("bairro"));
            posto.cidade = c.getString(c.getColumnIndex("cidade"));
            posto.uf = c.getString(c.getColumnIndex("uf"));
            posto.bandeira = c.getString(c.getColumnIndex("bandeira"));
            posto.gasolina = c.getString(c.getColumnIndex("gasolina"));
            posto.gnv = c.getString(c.getColumnIndex("gnv"));
            posto.alcool = c.getString(c.getColumnIndex("alcool"));
            posto.latitude = c.getString(c.getColumnIndex("latitude"));
            posto.longitude = c.getString(c.getColumnIndex("longitude"));

            postos.add(posto);
        }
        return postos;
    }

}
