package br.com.lf.abasteceali;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by lf.fernandodossantos on 01/04/17.
 */

public class HelperMap implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, OnMapReadyCallback {
    private GoogleApiClient client;
    private Context context;
    GoogleMap mMap;
    private Double latGPS, lonGPS;
    private MaterialDialog mDialog;
    private static final int REQUEST_PERMISSIONS_CODE = 128;


    public HelperMap(Context context, GoogleApiClient client){
        this.client = client;
        this.context = context;
        this.mMap = mMap;

        capturaLocalizacao();
    }
     private synchronized void capturaLocalizacao() {
        client = new GoogleApiClient.Builder(context)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        client.connect();

    }

    public void reconectClient(){
        client.reconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(context,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(client);

        if (location != null){
            latGPS = location.getLatitude();
            lonGPS = location.getLongitude();

            Log.i("GPS", "LAT: " + String.valueOf(latGPS));
            Log.i("GPS", "LAT: " + String.valueOf(lonGPS));

            onMapReady(mMap);
            //runJSON();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (latGPS != null) {
            LatLng local = new LatLng(latGPS, lonGPS);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(local, 13));

            if (ActivityCompat.checkSelfPermission(context,
                    android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                callAcessLocation();
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    private void callAcessLocation() {
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                callDialog("É necessário habilitar as permissões para utilizar o App.", new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION});
            } else {

                ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_CODE);
            }
        }
    }

    public void callDialog(final String msg, final String[] permissions) {

        mDialog = new MaterialDialog(context)
                .setTitle("Permission")
                .setMessage(msg)
                .setPositiveButton(" OK ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityCompat.requestPermissions((Activity) context, permissions, REQUEST_PERMISSIONS_CODE);
                        mDialog.dismiss();

                    }
                })
                .setNegativeButton(" Cancel ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                    }
                });

        mDialog.show();
    }
}
