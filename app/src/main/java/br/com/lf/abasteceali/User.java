package br.com.lf.abasteceali;

/**
 * Created by lf.fernandodossantos on 28/03/17.
 */

public class User {

    public String user;
    public String comentario;

    public String getComentario() {
        return comentario;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }


}
