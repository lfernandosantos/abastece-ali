package br.com.lf.abasteceali;

import android.*;
import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.lf.abasteceali.dao.PostoDAO;
import br.com.lf.abasteceali.models.Posto;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NoLoginActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{


    private GoogleMap mMap;
    private GoogleApiClient client;
    private MaterialDialog mDialog;
    private static final int REQUEST_PERMISSIONS_CODE = 128;
    private Double latGPS, lonGPS;
    private List<Posto> postoList, postosOtherPlace;
    private FloatingActionButton buttonGetMyLocation;
    private EditText editTextOtherPlace;
    private ImageView imageViewSearchOtherPlace;
    ProgressDialog progressDialog;
    private String endereco = null;
    private RadioGroup radioGroup;
    private Boolean radioGnv = false, radioAlcool =false, offline = false;
    private int lastIdRadio = 0;
    private HashMap<Marker, Posto> eventMarkerMap;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_login);

        findViews();
        //verificando versão
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Call some material design APIs here
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            buttonGetMyLocation = (FloatingActionButton) findViewById(R.id.buttonGetMyLocation);
            buttonGetMyLocation.setOnClickListener(this);
            imageViewSearchOtherPlace.setOnClickListener(this);
        }

        //colocando mapa no fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.frame_mapa);
        mapFragment.getMapAsync(this);

        //aguardando consulta da api
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Aguarde...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        postosOtherPlace = null;

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                lastIdRadio = checkedId;

                progressDialog.show();
                switch (checkedId){
                    case R.id.radioAll: Log.i("RADIO","ALL");
                        if (lastIdRadio != R.id.radioAll) {
                            pegaAll();
                        }
                        break;
                    case R.id.radioAlcool: Log.i("RADIO","ALCOOL");
                        if (lastIdRadio != R.id.radioAlcool) {
                            pegaApenasAlcool();
                        }
                        break;
                    case R.id.radioGNV: Log.i("RADIO","GNV");
                        if (lastIdRadio != R.id.radioGNV) {
                            pegaApenasGNV();
                        }
                        break;
                }
            }
        });

        editTextOtherPlace.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){

                    searchOtherPlace();

                    return true;
                }
                return false;
            }
        });



    }

    private void findViews() {
        editTextOtherPlace = (EditText) findViewById(R.id.editTextOtherPlace);
        imageViewSearchOtherPlace = (ImageView) findViewById(R.id.imageSearchToolbar);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
    }


    @Override
    protected void onResume() {
        super.onResume();


        if (verificaGPS()) {

            if (verificaConexao()) {
                //exibe dialog enquanto o device captura a localização
                progressDialog.show();
                offline = false;
                radioGroup.setVisibility(View.VISIBLE);
                capturaLocalizacao();

            }else{
                //captura última localizacao do device para exibir postos
                progressDialog.show();
                offline = true;
                radioGroup.setVisibility(View.GONE);
                capturaLocalizacao();
            }

        }else {
            new AlertDialog.Builder(this)
                    .setTitle("Erro de Localização")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .setMessage(R.string.erro_gps_alta_precisao)
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }
    }

    private void runJSON(final boolean otherPlace, final LatLng latLng, boolean gnv, boolean alcool) {

        postoList = new ArrayList<>();

        if(offline){

            PostoDAO dao = new PostoDAO(this);

            postoList = dao.getPostos();
            if (postoList.size() > 0) {
                addMarker(postoList);
            }else {
                Snackbar.make(radioGroup, "Sem postos offline!", Snackbar.LENGTH_LONG).show();
            }

        }else {

            OkHttpClient okHttpClient = getRequestHeader();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(IPostosJSON.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            IPostosJSON iPostosJSON = retrofit.create(IPostosJSON.class);

            final String latitude = String.valueOf(latLng.latitude);
            String longitude = String.valueOf(latLng.longitude);

            Call<Catalogo> listaCall;

                if (gnv) {
                    listaCall = iPostosJSON.listaPostosLocationGNV(latitude, longitude, "1");
                    Log.i("TAG", "Call: GNV");
                } else if (alcool) {
                    listaCall = iPostosJSON.listaPostosLocationAlcool(latitude, longitude, "1");
                    Log.i("TAG", "Call: Alcool");
                } else {
                    listaCall = iPostosJSON.listaPostosLocation(latitude, longitude);
                    Log.i("TAG", "Call: All");
                }


            Log.i("TAG", "Call: ");
            listaCall.enqueue(new Callback<Catalogo>() {
                @Override
                public void onResponse(Call<Catalogo> call, Response<Catalogo> response) {

                    if (!response.isSuccessful()) {
                        Log.i("TAG", "ERRO: " + response.code());
                    } else {
                        Catalogo catalogo = response.body();

                        for (Posto p : catalogo.postos) {
                            Log.i("TAG", "Posto: " + p.posto);
                            postoList.add(p);
                        }
                        mMap.clear();
                        addMarker(postoList);
                        salvarPostos();

                        if(otherPlace) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
                            Localizador localizador = new Localizador(NoLoginActivity.this);
                            Address address = localizador.getEnderecoString(latLng);
                            MarkerOptions markerOptionsLocal = new MarkerOptions();

                            if (address != null) {
                                markerOptionsLocal.position(latLng).title(address.getAddressLine(1))
                                        .snippet(address.getAddressLine(0))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_radius));
                            } else {
                                markerOptionsLocal.position(latLng).title(latitude.toString())
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_radius));
                            }
                            mMap.addMarker(markerOptionsLocal);

                        }
                        progressDialog.dismiss();

                    }

                }

                @Override
                public void onFailure(Call<Catalogo> call, Throwable t) {

                    progressDialog.dismiss();

                    Log.i("TAG", "ERRO: " + t.getMessage());

                    new AlertDialog.Builder(NoLoginActivity.this)
                            .setTitle("Erro de Comunicação")
                            .setMessage(R.string.erro_comunicacao)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setCancelable(false)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).create().show();
                }
            });

        }
    }

    //salvar postos no localstorage
    private void salvarPostos() {

        PostoDAO dao = new PostoDAO(getApplicationContext());

        for (Posto p : postoList){
            dao.savePosto(p);
            Log.i("SQLITE", "Posto " + p.posto + " salvo!");
        }
        dao.close();
    }

    private void addMarker(List<Posto> postoList) {

        eventMarkerMap = new HashMap<>();
        for (int i = 0 ; i < postoList.size(); i++) {

            LatLng coordenada = new LatLng(Double.valueOf(postoList.get(i).latitude), Double.valueOf(postoList.get(i).longitude));

            //adicinonando marker ao HashMap
            addMarkerOptions(postoList, i, coordenada);

            if (i == postoList.size()-1){
                if (progressDialog != null) {
                    if (progressDialog.isShowing()) {
                        progressDialog.incrementProgressBy(5);
                        progressDialog.dismiss();

                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(final Marker marker) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(NoLoginActivity.this);

                                builder = DialogInflaterHelper
                                        .inflateAlertDialogPosto(builder, NoLoginActivity.this, marker, eventMarkerMap);
                                builder.create().show();
                                return false;
                            }
                        });
                    }
                }
            }
        }
    }

    private void addMarkerOptions(List<Posto> postoList, int i, LatLng coordenada) {
        if (coordenada != null) {
            MarkerOptions markerOptions = new MarkerOptions();

            markerOptions.position(coordenada).title(postoList.get(i).posto).snippet(postoList.get(i).endereco)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));

            Marker marker = mMap.addMarker(markerOptions);

            eventMarkerMap.put(marker, postoList.get(i));


        }
    }

    private synchronized void capturaLocalizacao() {
                client = new GoogleApiClient.Builder(this)
                        .addOnConnectionFailedListener(this)
                        .addConnectionCallbacks(this)
                        .addApi(LocationServices.API)
                        .build();
                client.connect();

    }

    private OkHttpClient getRequestHeader() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(25, TimeUnit.SECONDS)
                .connectTimeout(35, TimeUnit.SECONDS)
                .build();

        return okHttpClient;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        if (latGPS != null) {
            LatLng local = new LatLng(latGPS, lonGPS);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(local, 13));

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                callAcessLocation();
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    private void pegaAll() {
        if (postosOtherPlace != null) {
            if (endereco != null) {
                buscaOtherPlace(endereco, false, false);
            }
        }else{
            client.reconnect();
            radioAlcool = false;
            radioGnv = false;
        }
    }

    private void pegaApenasGNV() {

        if (postosOtherPlace != null) {
            if (endereco != null) {
                buscaOtherPlace(endereco, true, false);
            }
        }else{
            client.reconnect();
            radioGnv = true;
            radioAlcool = false;
        }
    }

    private void pegaApenasAlcool() {
        if (postosOtherPlace != null) {
            if (endereco != null) {
                buscaOtherPlace(endereco, false, true);
            }
        }else{
            client.reconnect();
            radioAlcool = true;
            radioGnv = false;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
               esconderTeclado();
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            callAcessLocation();
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(client);

        if (location == null){
            client.connect();
        }

        if (location != null){
            latGPS = location.getLatitude();
            lonGPS = location.getLongitude();

            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

            Log.i("GPS", "LAT: " + String.valueOf(latLng.latitude));
            Log.i("GPS", "LAT: " + String.valueOf(latLng.longitude));

            onMapReady(mMap);
            runJSON(false, latLng, radioGnv, radioAlcool);

        }else {

            new AlertDialog.Builder(NoLoginActivity.this)
                    .setTitle("Alerta")
                    .setMessage(R.string.erro_comunicacao)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).create().show();
        }
    }

    private void callAcessLocation() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                callDialog("É necessário habilitar as permissões para utilizar o App.", new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION});
            } else {

                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_CODE);
            }
        }
    }

    public void callDialog(final String msg, final String[] permissions) {

        mDialog = new MaterialDialog(this)
                .setTitle("Permissão")
                .setMessage(msg)
                .setPositiveButton(" OK ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityCompat.requestPermissions(NoLoginActivity.this, permissions, REQUEST_PERMISSIONS_CODE);
                        mDialog.dismiss();

                    }
                })
                .setNegativeButton(" Cancel ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();

                    }
                });

        mDialog.show();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        new AlertDialog.Builder(NoLoginActivity.this)
                .setTitle("Alerta")
                .setMessage(R.string.erro_comunicacao)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).create().show();
    }

    public void buscaOtherPlace(String endereco, Boolean gnv, Boolean alcool){

        Localizador localizador = new Localizador(this);
        LatLng otherPlace = localizador.getCoordenada(endereco);
        Double latOtherPlace = null;
        Double lonOtherPlace = null;
        if (otherPlace != null || !endereco.equals("")) {
            try {
                 latOtherPlace = otherPlace.latitude;
                 lonOtherPlace = otherPlace.longitude;
            }catch (Exception e){

                alertEndInvalido();
            }
            if (latOtherPlace != null && lonOtherPlace != null) {
                mMap.clear();
                runJSON(true, otherPlace, gnv, alcool);
                //runJSONOther(otherPlace, gnv, alcool);
            }
        } else {
            alertEndInvalido();
        }
    }

    private void alertEndInvalido() {
        progressDialog.dismiss();
        new AlertDialog.Builder(NoLoginActivity.this)
                .setTitle("Endereço Inválido")
                .setMessage(R.string.erro_localizacao)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setNeutralButton("OK", null).create().show();
    }

//    public void runJSONOther(final LatLng endereco, Boolean gnv, Boolean alccol) {
//
//        postosOtherPlace = new ArrayList<>();
//
//        String latGPS = String.valueOf(endereco.latitude);
//        String lonGPS = String.valueOf(endereco.longitude);
//
//        OkHttpClient okHttpClient = getRequestHeader();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(IPostosJSON.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(okHttpClient)
//                .build();
//
//        IPostosJSON iPostosJSON = retrofit.create(IPostosJSON.class);
//
//        String latitude = String.valueOf(latGPS);
//        final String longitude = String.valueOf(lonGPS);
//
//        Call<Catalogo> listaCall;
//
//        if (gnv) {
//            listaCall = iPostosJSON.listaPostosLocationGNV(latitude, longitude, "1");
//            Log.i("TAG", "Call: GNV");
//        } else if (alccol) {
//            listaCall = iPostosJSON.listaPostosLocationAlcool(latitude, longitude, "1");
//            Log.i("TAG", "Call: GNV");
//        } else {
//            listaCall = iPostosJSON.listaPostosLocation(latitude, longitude);
//            Log.i("TAG", "Call: All ");
//        }
//
//        listaCall.enqueue(new Callback<Catalogo>() {
//            @Override
//            public void onResponse(Call<Catalogo> call, Response<Catalogo> response) {
//                if (!response.isSuccessful()) {
//                    Log.i("TAG", "ERRO: " + response.code());
//                } else {
//                    Catalogo catalogo = response.body();
//
//                    for (Posto p : catalogo.postos) {
//                        Log.i("TAG", "Posto: " + p.posto);
//                        postosOtherPlace.add(p);
//                    }
//                    mMap.clear();
//
//                    addMarker(postosOtherPlace);
//
//                    //salvar postos
//                    PostoDAO dao = new PostoDAO(getApplicationContext());
//
//                    for (Posto p : postosOtherPlace){
//                        dao.savePosto(p);
//                    }
//
//                    dao.close();
//
//                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(endereco, 13));
//                    Localizador localizador = new Localizador(NoLoginActivity.this);
//                    Address address = localizador.getEnderecoString(endereco);
//                    MarkerOptions markerOptionsLocal = new MarkerOptions();
//
//                    if (address != null){
//                        markerOptionsLocal.position(endereco).title(address.getAddressLine(1))
//                                .snippet(address.getAddressLine(0))
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_radius));
//                    }else {
//                        markerOptionsLocal.position(endereco).title(endereco.toString())
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_radius));
//                    }
//                    mMap.addMarker(markerOptionsLocal);
//
//
//                    progressDialog.dismiss();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Catalogo> call, Throwable t) {
//                Log.i("TAG", "ERRO: " + t.getMessage());
//
//                progressDialog.dismiss();
//                new AlertDialog.Builder(NoLoginActivity.this)
//                        .setTitle("Erro de Comunicação")
//                        .setMessage("Houve erro ao buscar os postos. Verifique seu GPS, sua conexão e tente novamente!")
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .setCancelable(false)
//                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        }).create().show();
//            }
//        });
//    }

    @Override
    public void onClick(View v) {

        if(verificaGPS()){

            if (verificaConexao()){
                offline = false;
            }else {
                offline = true;
            }

            if (v == buttonGetMyLocation) {
                buttonGetMyLocation.setEnabled(false);
                progressDialog.show();
                postosOtherPlace = null;
                client.reconnect();
                radioGroup.check(R.id.radioAll);
                buttonGetMyLocation.setEnabled(true);
            }

            if (v == imageViewSearchOtherPlace) {

                searchOtherPlace();
            }

        }else {

            new AlertDialog.Builder(this)
                    .setTitle("Erro de Localização")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.erro_gps_alta_precisao)
                    .setNeutralButton("OK", null)
                    .show();

        }
    }

    private void searchOtherPlace() {
        if (!offline) {
            if (editTextOtherPlace.getText().toString().isEmpty() || editTextOtherPlace.getText().equals(" ")) {
                editTextOtherPlace.requestFocus();
                exibirTeclado();
            } else {
                imageViewSearchOtherPlace.setEnabled(false);
                progressDialog.show();
                endereco = editTextOtherPlace.getText().toString().trim();
                buscaOtherPlace(endereco, false, false);
                esconderTeclado();
                imageViewSearchOtherPlace.setEnabled(true);
            }
        }else {
            new AlertDialog.Builder(this)
                    .setTitle("Modo Offline")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setMessage(R.string.erro_modo_offline)
                    .setNeutralButton("OK", null)
                    .show();
        }
    }

    private void esconderTeclado() {
        editTextOtherPlace.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editTextOtherPlace.getWindowToken(), 0);
    }
    private void exibirTeclado() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(editTextOtherPlace, 0);
    }

    public Boolean verificaGPS(){
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Boolean statusGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        return statusGPS;
    }

    public  Boolean verificaConexao(){
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        Boolean statusNET;

        if (info != null && info.isConnectedOrConnecting() && info.isAvailable()){
            statusNET = true;
        }else {
            statusNET = false;
        }

        return  statusNET;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
