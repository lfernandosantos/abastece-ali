package br.com.lf.abasteceali;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by lf.fernandodossantos on 01/04/17.
 */

public abstract class  Helper {




    private static void esconderTeclado(Context context, View v) {
        v.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    private static void exibirTeclado(Context context, View v) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(v, 0);
    }

    public static Boolean verificaGPS(Context context){
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Boolean statusGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        return statusGPS;
    }

    public  static Boolean verificaConexao(Context context){
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        Boolean statusNET;

        if (info != null && info.isConnectedOrConnecting() && info.isAvailable()){
            statusNET = true;
        }else {
            statusNET = false;
        }

        return  statusNET;
    }
}
