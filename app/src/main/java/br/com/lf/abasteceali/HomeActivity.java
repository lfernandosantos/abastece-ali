package br.com.lf.abasteceali;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.concurrent.TimeUnit;

import br.com.lf.abasteceali.models.Posto;
import br.com.lf.abasteceali.models.Users;
import me.drakeet.materialdialog.MaterialDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private GoogleApiClient client;



        private FirebaseAuth firebaseAuth;
        private FirebaseUser user;

        private ImageView photoProfile;
        private TextView nameProfile;

        private RadioGroup radioGroup;
        private int lastIdRadio = 0;
        private List<Posto> postosOtherPlace;
        private String endereco = null;
        private Boolean radioGnv = false, radioAlcool = false;
        private Double latGPS, lonGPS;

        private List<Posto> postoList;

        private HelperMap helperMap;
        private GoogleMap mMap;

        private EditText editTextOtherPlace;
        private ImageView imageViewSearchOtherPlace;
        private FloatingActionButton buttonGetMyLocation;

        private HashMap<Marker, Posto> eventMarkerMap;
        ProgressDialog progressDialog;

        private MaterialDialog mDialog;
        private static final int REQUEST_PERMISSIONS_CODE = 128;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.drawer_home_layout);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

            setSupportActionBar(toolbar);

            toolbar.setNavigationIcon(R.mipmap.ic_menu);

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.frame_mapa_home);
            mapFragment.getMapAsync(this);



            firebaseAuth = FirebaseAuth.getInstance();
            if (firebaseAuth.getCurrentUser() == null) {
                finish();
                startActivity(new Intent(this, LoginActivity.class));
            }

            // helperMap = new HelperMap(this, client);


            //changedRadios();

            user = firebaseAuth.getCurrentUser();

            DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_homelayout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout,
                    toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            toggle.setHomeAsUpIndicator(R.mipmap.ic_menu);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_home);
            navigationView.setNavigationItemSelectedListener(this);


            View view = getLayoutInflater().inflate(R.layout.nav_header_home, navigationView);
            photoProfile = (ImageView) view.findViewById(R.id.nav_photo_perfil);
            nameProfile = (TextView) view.findViewById(R.id.nav_name_perfil);


            Picasso.with(this).load(user.getPhotoUrl()).into(photoProfile);
            nameProfile.setText(user.getDisplayName());

            final FirebaseDatabase database = FirebaseDatabase.getInstance();

            String idPosto;

            idPosto = "101";

            DatabaseReference refGet = database.getReference("server/saving-data/abasteceali/postos/101/");

            // Attach a listener to read the data at our posts reference
            refGet.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    HashMap<String, String> values = (HashMap<String, String>) dataSnapshot.getValue();

                    Set<Map.Entry<String, String>> faceUsers = values.entrySet();

                    for (Map.Entry<String, String> bla : faceUsers) {

                        System.out.println(bla.getKey() + ", " + bla.getValue());
                    }


                    //GenericTypeIndicator<HashMap<String,String>> t = new GenericTypeIndicator<HashMap<String, String>>() {};
//                    HashMap<String, String> faceUser = (HashMap<String, String>) dataSnapshot.getValue();
//                    // List<String> faceUses = (List<String>) dataSnapshot.getValue();
//
//                    List<Users> userList = new ArrayList<Users>();
//                    Set<Map.Entry<String, String>> users = faceUser.entrySet();

//                    for (Map.Entry<String, String> bla : users) {
//                        //userList.add(new Users(bla.getKey(), bla.getValue()));
//
//                        System.out.println("chave = " + bla.getKey() + "  valor = " + bla.getValue());
//                    }


                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("The read failed: " + databaseError.getCode());
                }
            });

            List<User> usuarios = new ArrayList<>();

            setComentario(usuarios, database, idPosto);

            findViews();

            capturaLocalizacao();

        }

        private void findViews() {
            editTextOtherPlace = (EditText) findViewById(R.id.editTextOtherPlace);
            imageViewSearchOtherPlace = (ImageView) findViewById(R.id.imageSearchToolbar);
            buttonGetMyLocation = (FloatingActionButton) findViewById(R.id.buttonGetMyLocation);
            radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        }

        @Override
        protected void onResume() {
            super.onResume();

            //helperMap.reconectClient();


            if (verificaConexao()) {
                if (verificaGPS()) {

                    capturaLocalizacao();

                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("Erro de Localização")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setCancelable(false)
                            .setMessage("O GPS deve estar ligado e em alta precisão para utilizar oo aplicativo!")
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            })
                            .show();
                }
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("Erro de Conexão")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setCancelable(false)
                        .setMessage("Verifique sua conexão e tente novamente!")
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .show();
            }
        }

        private void setComentario(List<User> lisUsers, FirebaseDatabase database, String idPosto) {
            User userFace = new User();


            userFace.setUser("1234005");
            userFace.setComentario("A gasolina desse posto não presta!");

            //lisUsers.add(userFace);
            userFace.setUser("12ii3096");
            userFace.setComentario("A gasolina desse posto teste");

            DatabaseReference ref = database.getReference("server/saving-data/abasteceali/postos/101");
            DatabaseReference usersRef = ref;

            usersRef.child(userFace.getUser()).setValue(userFace.getComentario());
        }


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.item_nav_favoritos) {
                Toast.makeText(this, "Favoritos!!", Toast.LENGTH_SHORT).show();
            } else if (id == R.id.item_nav_sair) {
                LoginManager.getInstance().logOut();
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
            }

            DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_homelayout);
            drawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }

        @Override
        public void onBackPressed() {
            DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_homelayout);
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }

        private void runJSON() {

            postoList = new ArrayList<>();

            OkHttpClient okHttpClient = getRequestHeader();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(IPostosJSON.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            IPostosJSON iPostosJSON = retrofit.create(IPostosJSON.class);

            String latitude = String.valueOf(latGPS);
            String longitude = String.valueOf(lonGPS);

            Call<Catalogo> listaCall;

            if (radioGnv) {
                listaCall = iPostosJSON.listaPostosLocationGNV(latitude, longitude, "1");
                Log.i("TAG", "Call: GNV");
            } else if (radioAlcool) {
                listaCall = iPostosJSON.listaPostosLocationAlcool(latitude, longitude, "1");
                Log.i("TAG", "Call: Alcool");
            } else {
                listaCall = iPostosJSON.listaPostosLocation(latitude, longitude);
                Log.i("TAG", "Call: All");
            }

            Log.i("TAG", "Call: ");
            listaCall.enqueue(new Callback<Catalogo>() {
                @Override
                public void onResponse(Call<Catalogo> call, Response<Catalogo> response) {

                    if (!response.isSuccessful()) {
                        Log.i("TAG", "ERRO: " + response.code());
                    } else {
                        Catalogo catalogo = response.body();

                        for (Posto p : catalogo.postos) {
                            Log.i("TAG", "Posto: " + p.posto);
                            postoList.add(p);
                        }
                        mMap.clear();
                        addMarker(postoList);
                    }

                }

                @Override
                public void onFailure(Call<Catalogo> call, Throwable t) {

                    progressDialog.dismiss();

                    Log.i("TAG", "ERRO: " + t.getMessage());

                    new AlertDialog.Builder(HomeActivity.this)
                            .setTitle("Erro de Comunicação")
                            .setMessage("Houve erro ao buscar os postos. Verifique seu GPS, sua conexão e tente novamente!")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setCancelable(false)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).create().show();
                }
            });

        }

        private void addMarker(List<Posto> postoList) {

            eventMarkerMap = new HashMap<>();
            for (int i = 0; i < postoList.size(); i++) {

                LatLng coordenada = new LatLng(Double.valueOf(postoList.get(i).latitude), Double.valueOf(postoList.get(i).longitude));
                if (coordenada != null) {
                    MarkerOptions markerOptions = new MarkerOptions();

                    markerOptions.position(coordenada).title(postoList.get(i).posto).snippet(postoList.get(i).endereco)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));


                    Marker marker = mMap.addMarker(markerOptions);

                    eventMarkerMap.put(marker, postoList.get(i));

                    Log.i("MARKER", markerOptions.getTitle());
                }
                if (i == postoList.size() - 1) {
                    if (progressDialog != null) {
                        if (progressDialog.isShowing()) {
                            progressDialog.incrementProgressBy(5);
                            progressDialog.dismiss();

                            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(final Marker marker) {
                                    inflateAlertDialogPosto(marker);
                                    return false;
                                }
                            });
                        }
                    }
                }
            }
        }

        private void inflateAlertDialogPosto(final Marker marker) {
            Posto p = eventMarkerMap.get(marker);
            if (p != null) {
                Log.i("Event", "Marker + posto " + p.gasolina);

                final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                LayoutInflater inflater = this.getLayoutInflater();
                View view = inflater.inflate(R.layout.layout_detail_posto, null);

                TextView textViewEnd = (TextView) view.findViewById(R.id.textEndereco);
                TextView textViewBairro = (TextView) view.findViewById(R.id.textBairro);
                TextView textViewDistancia = (TextView) view.findViewById(R.id.text_distancia);
                ImageView imgGas = (ImageView) view.findViewById(R.id.img_gas_posto_detail);
                ImageView imgEtanol = (ImageView) view.findViewById(R.id.img_etenol_posto_detail);
                ImageView imgGnv = (ImageView) view.findViewById(R.id.img_gnv_posto_detail);
                ImageView imgGoMaps = (ImageView) view.findViewById(R.id.img_navegation_GoogleMaps);

                textViewEnd.setText(p.endereco);
                textViewBairro.setText(p.bairro);

                DecimalFormat df = new DecimalFormat("#,###.00");
                String d = df.format(Double.valueOf(p.distancia));

                textViewDistancia.setText("Distância: " + d + " KM");

                imgGas.getLayoutParams().width = 70;
                imgGas.getLayoutParams().height = 70;

                imgEtanol.getLayoutParams().width = 70;
                imgEtanol.getLayoutParams().height = 70;

                imgGnv.getLayoutParams().width = 70;
                imgGnv.getLayoutParams().height = 70;

                imgGoMaps.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LatLng markerLocation = marker.getPosition();

                        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + markerLocation.latitude + "," + markerLocation.longitude);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                });

                Glide.with(this).load(R.drawable.ic_img_gas_on).into(imgGas);
                if (!p.alcool.equals("0")) {
                    Glide.with(this).load(R.drawable.ic_etanol).into(imgEtanol);
                }
                if (!p.gnv.equals("0")) {
                    Glide.with(this).load(R.drawable.ic_img_gnv_on).into(imgGnv);
                }

                if (p.bandeira.equals("PETROBRAS DISTRIBUIDORA S.A.")) {
                    Glide.with(this)
                            .load("https://logodownload.org/wp-content/uploads/2014/05/br-distribuidora-logo.png")
                            .into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    builder.setIcon(resource);
                                }
                            });
                }
                if (p.bandeira.equals("RAIZEN")) {
                    Glide.with(this)
                            .load("https://logodownload.org/wp-content/uploads/2014/06/Raizen-logo.png")
                            .into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    builder.setIcon(resource);
                                }
                            });
                }
                if (p.bandeira.equals("IPIRANGA")) {
                    Glide.with(this)
                            .load("https://pbs.twimg.com/profile_images/562961424335392768/whrASwNT.png")
                            .into(new SimpleTarget<GlideDrawable>() {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                    builder.setIcon(resource);
                                }
                            });
                }
                builder.setView(view);
                builder.setTitle(p.posto);
                builder.setNeutralButton("Cancelar", null);
                builder.create().show();

            }
        }

        private synchronized void capturaLocalizacao() {
            client = new GoogleApiClient.Builder(this)
                    .addOnConnectionFailedListener(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
            client.connect();

        }

        private OkHttpClient getRequestHeader() {

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

            return okHttpClient;
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMap = googleMap;
            if (latGPS != null) {
                LatLng local = new LatLng(latGPS, lonGPS);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(local, 13));

                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    callAcessLocation();
                }
                mMap.setMyLocationEnabled(true);
            }
        }

        private void pegaAll() {
            if (postosOtherPlace != null) {
                if (endereco != null) {
                    buscaOtherPlace(endereco, false, false);
                }
            } else {
                client.reconnect();
                radioAlcool = false;
                radioGnv = false;
            }
        }

        private void pegaApenasGNV() {

            if (postosOtherPlace != null) {
                if (endereco != null) {
                    buscaOtherPlace(endereco, true, false);
                }
            } else {
                client.reconnect();
                radioGnv = true;
                radioAlcool = false;
            }
        }

        private void pegaApenasAlcool() {
            if (postosOtherPlace != null) {
                if (endereco != null) {
                    buscaOtherPlace(endereco, false, true);
                }
            } else {
                client.reconnect();
                radioAlcool = true;
                radioGnv = false;
            }
        }

        @Override
        public void onConnected(@Nullable Bundle bundle) {
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                esconderTeclado();
            }
        });
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                callAcessLocation();
            }

            Location location = LocationServices.FusedLocationApi.getLastLocation(client);

            if (location != null) {
                latGPS = location.getLatitude();
                lonGPS = location.getLongitude();

                Log.i("GPS", "LAT: " + String.valueOf(latGPS));
                Log.i("GPS", "LAT: " + String.valueOf(lonGPS));

                onMapReady(mMap);
                runJSON();
            }
        }

        private void callAcessLocation() {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                    callDialog("É necessário habilitar as permissões para utilizar o App.", new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION});
                } else {

                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_CODE);
                }
            }
        }

        public void callDialog(final String msg, final String[] permissions) {

            mDialog = new MaterialDialog(this)
                    .setTitle("Permission")
                    .setMessage(msg)
                    .setPositiveButton(" OK ", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ActivityCompat.requestPermissions(HomeActivity.this, permissions, REQUEST_PERMISSIONS_CODE);
                            mDialog.dismiss();

                        }
                    })
                    .setNegativeButton(" Cancel ", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();

                        }
                    });

            mDialog.show();
        }

        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        }

        public void buscaOtherPlace(String endereco, Boolean gnv, Boolean alcool) {

            Localizador localizador = new Localizador(this);
            LatLng otherPlace = localizador.getCoordenada(endereco);
            Double latOtherPlace = null;
            Double lonOtherPlace = null;
            if (otherPlace != null || !endereco.equals("")) {
                try {
                    latOtherPlace = otherPlace.latitude;
                    lonOtherPlace = otherPlace.longitude;
                } catch (Exception e) {

                    alertEndInvalido();
                }
                if (latOtherPlace != null && lonOtherPlace != null) {
                    mMap.clear();
                    runJSONOther(otherPlace, gnv, alcool);
                }
            } else {

                alertEndInvalido();
            }
        }

        private void alertEndInvalido() {
            progressDialog.dismiss();
            new AlertDialog.Builder(this)
                    .setTitle("Endereço Inválido")
                    .setMessage("Verifique o endereço informado e tente novamente!")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .setNeutralButton("OK", null).create().show();
        }

        public void runJSONOther(final LatLng endereco, Boolean gnv, Boolean alccol) {

            postosOtherPlace = new ArrayList<>();

            String latGPS = String.valueOf(endereco.latitude);
            String lonGPS = String.valueOf(endereco.longitude);

            OkHttpClient okHttpClient = getRequestHeader();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(IPostosJSON.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            IPostosJSON iPostosJSON = retrofit.create(IPostosJSON.class);

            String latitude = String.valueOf(latGPS);
            final String longitude = String.valueOf(lonGPS);

            Call<Catalogo> listaCall;

            if (gnv) {
                listaCall = iPostosJSON.listaPostosLocationGNV(latitude, longitude, "1");
                Log.i("TAG", "Call: GNV");
            } else if (alccol) {
                listaCall = iPostosJSON.listaPostosLocationAlcool(latitude, longitude, "1");
                Log.i("TAG", "Call: GNV");
            } else {
                listaCall = iPostosJSON.listaPostosLocation(latitude, longitude);
                Log.i("TAG", "Call: All ");
            }

            listaCall.enqueue(new Callback<Catalogo>() {
                @Override
                public void onResponse(Call<Catalogo> call, Response<Catalogo> response) {
                    if (!response.isSuccessful()) {
                        Log.i("TAG", "ERRO: " + response.code());
                    } else {
                        Catalogo catalogo = response.body();

                        for (Posto p : catalogo.postos) {
                            Log.i("TAG", "Posto: " + p.posto);
                            postosOtherPlace.add(p);
                        }
                        mMap.clear();

                        addMarker(postosOtherPlace);

                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(endereco, 13));
                        Localizador localizador = new Localizador(HomeActivity.this);
                        Address address = localizador.getEnderecoString(endereco);
                        MarkerOptions markerOptionsLocal = new MarkerOptions();

                        if (address != null) {
                            markerOptionsLocal.position(endereco).title(address.getAddressLine(1))
                                    .snippet(address.getAddressLine(0))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_radius));
                        } else {
                            markerOptionsLocal.position(endereco).title(endereco.toString())
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker_radius));
                        }
                        mMap.addMarker(markerOptionsLocal);

                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Catalogo> call, Throwable t) {
                    Log.i("TAG", "ERRO: " + t.getMessage());

                    progressDialog.dismiss();
                    new AlertDialog.Builder(HomeActivity.this)
                            .setTitle("Erro de Comunicação")
                            .setMessage("Houve erro ao buscar os postos. Verifique seu GPS, sua conexão e tente novamente!")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setCancelable(false)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).create().show();
                }
            });
        }

        @Override
        public void onClick(View v) {
            if (verificaConexao()) {
                if (verificaGPS()) {
                    if (v == buttonGetMyLocation) {
                        buttonGetMyLocation.setEnabled(false);
                        progressDialog.show();
                        postosOtherPlace = null;
                        client.reconnect();
                        radioGroup.check(R.id.radioAll);
                        buttonGetMyLocation.setEnabled(true);
                    }
                    if (v == imageViewSearchOtherPlace) {

                        if (editTextOtherPlace.getText().toString().isEmpty() || editTextOtherPlace.getText().equals(" ")) {
                            editTextOtherPlace.requestFocus();
                            exibirTeclado();
                        } else {
                            imageViewSearchOtherPlace.setEnabled(false);
                            progressDialog.show();
                            endereco = editTextOtherPlace.getText().toString().trim();
                            buscaOtherPlace(endereco, false, false);
                            esconderTeclado();
                            imageViewSearchOtherPlace.setEnabled(true);
                        }
                    }
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("Erro de Localização")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setMessage("O GPS deve estar ligado e em alta precisão para utilizar o aplicativo!")
                            .setNeutralButton("OK", null)
                            .show();
                }
            } else {
                new AlertDialog.Builder(this)
                        .setTitle("Erro de Conexão")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setMessage("Verifique sua conexão e tente novamente!")
                        .setNeutralButton("OK", null)
                        .show();
            }
        }

        private void esconderTeclado() {
            editTextOtherPlace.clearFocus();
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editTextOtherPlace.getWindowToken(), 0);
        }

        private void exibirTeclado() {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(editTextOtherPlace, 0);
        }

        public Boolean verificaGPS() {
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            Boolean statusGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            return statusGPS;
        }

        public Boolean verificaConexao() {
            ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = manager.getActiveNetworkInfo();
            Boolean statusNET;

            if (info != null && info.isConnectedOrConnecting() && info.isAvailable()) {
                statusNET = true;
            } else {
                statusNET = false;
            }

            return statusNET;
        }
    }

