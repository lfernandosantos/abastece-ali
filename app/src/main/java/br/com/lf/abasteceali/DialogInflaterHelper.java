package br.com.lf.abasteceali;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.lang.ref.PhantomReference;
import java.text.DecimalFormat;
import java.util.HashMap;

import br.com.lf.abasteceali.models.Posto;

/**
 * Created by lf.fernandodossantos on 15/04/17.
 */

public class DialogInflaterHelper {

    public Activity activity;
    public Marker marker;
    public HashMap<Marker, Posto> eventMarkerMap;

    public DialogInflaterHelper(Activity activity, Marker marker, HashMap<Marker, Posto> eventMarkerMap) {
        this.activity = activity;
        this.marker = marker;
        this.eventMarkerMap = eventMarkerMap;
    }

    public DialogInflaterHelper(Activity activity) {
        this.activity = activity;
    }

    public static AlertDialog.Builder  inflateAlertDialogPosto(final AlertDialog.Builder builder, final Activity activity, final Marker marker, HashMap<Marker, Posto> eventMarkerMap) {

        AlertDialog.Builder dialog = null;

        Posto p = eventMarkerMap.get(marker);
        if(p != null) {
            Log.i("Event", "Marker + posto " + p.gasolina);



            final LayoutInflater inflater = activity.getLayoutInflater();
            View view = inflater.inflate(R.layout.layout_detail_posto, null);

            TextView textViewEnd = (TextView) view.findViewById(R.id.textEndereco);
            TextView textViewBairro = (TextView) view.findViewById(R.id.textBairro);
            TextView textViewDistancia = (TextView) view.findViewById(R.id.text_distancia);
            ImageView imgGas = (ImageView) view.findViewById(R.id.img_gas_posto_detail);
            ImageView imgEtanol = (ImageView) view.findViewById(R.id.img_etenol_posto_detail);
            ImageView imgGnv = (ImageView) view.findViewById(R.id.img_gnv_posto_detail);
            ImageView imgGoMaps = (ImageView) view.findViewById(R.id.img_navegation_GoogleMaps);

            textViewEnd.setText(p.endereco);
            textViewBairro.setText(p.bairro);

            if(p.distancia != null) {
                DecimalFormat df = new DecimalFormat("#,###.00");
                String d = df.format(Double.valueOf(p.distancia));

                textViewDistancia.setText("Distância: " + d + " KM");
            }
            imgGas.getLayoutParams().width = 70;
            imgGas.getLayoutParams().height = 70;

            imgEtanol.getLayoutParams().width = 70;
            imgEtanol.getLayoutParams().height = 70;

            imgGnv.getLayoutParams().width = 70;
            imgGnv.getLayoutParams().height = 70;

            imgGoMaps.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LatLng markerLocation = marker.getPosition();

                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + markerLocation.latitude + "," + markerLocation.longitude);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    activity.startActivity(mapIntent);
                }
            });

            Glide.with(activity.getApplicationContext()).load(R.drawable.ic_img_gas_on).into(imgGas);
            if (!p.alcool.equals("0")) {
                Glide.with(activity.getApplicationContext()).load(R.drawable.ic_etanol).into(imgEtanol);
            }
            if (!p.gnv.equals("0")) {
                Glide.with(activity.getApplicationContext()).load(R.drawable.ic_img_gnv_on).into(imgGnv);
            }

            if (p.bandeira.equals("PETROBRAS DISTRIBUIDORA S.A.")) {
                Glide.with(activity.getApplicationContext())
                        .load("https://logodownload.org/wp-content/uploads/2014/05/br-distribuidora-logo.png")
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                builder.setIcon(resource);
                            }
                        });
            }
            if (p.bandeira.equals("RAIZEN")) {
                Glide.with(activity.getApplicationContext())
                        .load("https://logodownload.org/wp-content/uploads/2014/06/Raizen-logo.png")
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                builder.setIcon(resource);
                            }
                        });
            }
            if (p.bandeira.equals("IPIRANGA")) {
                Glide.with(activity.getApplicationContext())
                        .load("https://pbs.twimg.com/profile_images/562961424335392768/whrASwNT.png")
                        .into(new SimpleTarget<GlideDrawable>() {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                builder.setIcon(resource);
                            }
                        });
            }
            builder.setView(view);
            builder.setTitle(p.posto);
            builder.setNeutralButton("Cancelar", null);

            dialog = builder;
        }

        return dialog;
    }

    public AlertDialog.Builder alertErrorComun(){

        AlertDialog.Builder  builder = new AlertDialog.Builder(activity.getBaseContext())
                .setTitle("Erro de Comunicação")
                .setMessage("Houve erro ao buscar os postos. Verifique seu GPS, sua conexão e tente novamente!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.finish();
                    }
                });

        return builder;
    }
}
