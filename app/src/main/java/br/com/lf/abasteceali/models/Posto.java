package br.com.lf.abasteceali.models;

/**
 * Created by ferna on 26/01/2017.
 */

public class Posto {

    public Long id;
    public String posto;

    public String nome;
    public String endereco;
    public String bairro;
    public String bandeira;
    public String cidade;
    public String uf;
    public String icone;
    public String gasolina;
    public String gnv;
    public String alcool;
    public String latitude;
    public String longitude;
    public String distancia;


    @Override
    public String toString() {
        return posto;
    }

}
